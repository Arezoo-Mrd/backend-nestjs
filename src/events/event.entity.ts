import {
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Attendee } from './attendee.entity';

@Entity('events', { name: 'events' })
export class Event {
  @PrimaryGeneratedColumn()
  id: number;
  // @PrimaryColumn('')

  @Column({ length: 100 })
  name: string;

  @Column()
  description: string;

  @Column()
  when: Date;

  @Column()
  address: string;

  @OneToMany(() => Attendee, (attendee) => attendee.event, {
    // eager: true
    cascade: true,
  })
  attendees: Attendee[];

  attendeeCount?: number;

  attendeeRejected?: number;
  attendeeMaybe?: number;
  attendeeAccepted?: number;
}
