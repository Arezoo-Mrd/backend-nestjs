import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Logger,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateEventDto } from './input/create-event.dto';
import { UpdateEventDto } from './input/update-event.dto';
import { Event } from './event.entity';
import { Like, MoreThan, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Attendee } from './attendee.entity';
import { EventsService } from './events.service';
import { ListEvents } from './input/list.events';

@Controller('/events')
export class EventController {
  private readonly logger = new Logger(EventController.name);
  constructor(
    @InjectRepository(Event)
    private readonly repository: Repository<Event>,

    @InjectRepository(Attendee)
    private readonly attendeeRepository: Repository<Attendee>,

    private readonly eventService: EventsService,
  ) {}
  @Get()
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  async findAll(@Query() filter: ListEvents) {
    this.logger.debug(`filter: ${filter.when}`);
    this.logger.log(`Hit the findAll route`);
    const events =
      await this.eventService.getEventEIthAttendeeCountFilterdPaginated(
        filter,
        {
          total: true,
          currentPage: filter.page,
          limit: 10,
        },
      );
    return events;
  }

  @Get('/practice')
  async practice() {
    return await this.repository.find({
      select: ['address', 'description', 'id'],
      where: [
        {
          id: MoreThan(3),
          when: MoreThan(new Date('2021-02-12T13:00:00')),
        },
        {
          description: Like('%meet%'),
        },
      ],
      take: 2,
      order: {
        id: 'DESC',
      },
    });
  }

  @Get('practice2')
  async practice2() {
    // return await this.repository.findOne({
    //   where: {
    //     id: 1
    //   }, relations: ['attendees'],
    //   loadEagerRelations: false
    // })
    const event = await this.repository.findOne({
      where: {
        id: 1,
      },
      relations: ['attendees'],
    });
    // const event = new Event()
    // event.id = 1
    const attendee = new Attendee();
    attendee.name = 'Using casCade';
    // attendee.event = event
    // event.attendees = []
    event.attendees.push(attendee);

    // await this.attendeeRepository.save(attendee)
    await this.repository.save(event);
    return event;
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const event = await this.eventService.getEvent(id);

    if (!event) {
      throw new NotFoundException();
    }
    return event;
  }

  @Post()
  async create(
    @Body(new ValidationPipe({ groups: ['create'] })) input: CreateEventDto,
  ) {
    return await this.repository.save({
      ...input,
      when: new Date(input.when),
    });
  }
  @Patch(':id')
  async update(
    @Param('id') id,
    @Body(new ValidationPipe({ groups: ['update'] })) input: UpdateEventDto,
  ) {
    const event = await this.repository.findOne(id);
    if (!event) {
      throw new NotFoundException();
    }

    return await this.repository.save({
      ...event,
      ...input,
      when: input.when ? new Date(input.when) : event.when,
    });
  }
  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id') id) {
    // const event = await this.repository.find(id);
    // if (!event) {
    //   throw new NotFoundException()
    // }
    const result = await this.eventService.deleteEvent(id);
    if (result.affected !== 1) {
      throw new NotFoundException();
    }
  }
}
