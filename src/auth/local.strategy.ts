import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { Repository } from "typeorm";

import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';

@Injectable()
export class LocalStartegy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(LocalStartegy.name);

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {
    super();
  }
  public async validate(username: string, password: string): Promise<any> {
    const user = await this.userRepository.findOne({
      where: {
        username,
      },
    });
    if (!user) {
      this.logger.debug(`this user cant be find ${username}`);
      throw new UnauthorizedException();
    }
    if (password !== user.password) {
      this.logger.debug(`Invalid credential for user ${username}`);
      throw new UnauthorizedException();
    }

    return user;
  }
}
