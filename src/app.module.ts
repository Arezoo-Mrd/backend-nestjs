import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsModule } from './events/events.module';
import { EventController } from './events/events.controller';
import { AppJapanService } from './events/app.japan.service';
import { ConfigModule } from '@nestjs/config';
import ormConfig from 'orm.config';
import ormConfigProd from 'orm.config.prod';
import { SchoolModule } from './school/school.module';
import { AuthModule } from './auth/auth.module';

@Module({
  // static module
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [ormConfig],
      expandVariables: true,
    }),
    TypeOrmModule.forRootAsync({
      useFactory:
        process.env.NODE_ENV !== 'production' ? ormConfig : ormConfigProd,
    }),
    AuthModule,
    EventsModule,
    SchoolModule,
  ],
  controllers: [AppController],
  // providers: [AppService],

  providers: [
    { provide: AppService, useClass: AppJapanService },
    { provide: 'APP_NAME', useValue: 'NEST events backend' },
  ],
})
export class AppModule {}
